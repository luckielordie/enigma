#ifndef ENIGMA_GAMEOBJECT
#define ENIGMA_GAMEOBJECT

#include <vector>

namespace Enigma
{
	class Component;
	class GameObject
	{
	private:
		std::string m_objectTag;
		std::vector<Component*>* m_pComponentVector = 0;
	public:
		GameObject(std::string objectTag);
		void AddComponent(Component* component);		
	};
}

#endif