#ifndef ENIGMA_COMPONENTS_COMPONENT
#define ENIGMA_COMPONENTS_COMPONENT

#include <GameObject.h>

namespace Enigma
{
	namespace Components
	{
		class Component
		{
		protected:
			Enigma::GameObject* m_pParentObject;
		public:
			Component(Enigma::GameObject* parentObject);
		};
	}
}

#endif