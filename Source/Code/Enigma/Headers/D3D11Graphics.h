#ifndef ENIGMA_GRAPHICS_D3D11GRAPHICS_H
#define ENIGMA_GRAPHICS_D3D11GRAPHICS_H

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>

namespace Enigma
{
	namespace Components
	{
		class Mesh;
	}

	namespace Graphics
	{
		class D3D11Graphics
		{
		private:
			D3D_DRIVER_TYPE         m_driverType = D3D_DRIVER_TYPE_NULL;
			D3D_FEATURE_LEVEL       m_featureLevel = D3D_FEATURE_LEVEL_11_1;
			ID3D11Device*           m_pD3DDevice;
			ID3D11DeviceContext*    m_pImmediateContext;

			D3D11Graphics();
		public:
			static D3D11Graphics& Instance();
			bool Initialise();
			void ClearBackBuffers();
			void SwapRenderBuffers();
			void RenderMesh(Enigma::Components::Mesh* mesh);
		};

		typedef D3D11Graphics D3D11;
	}
}
#endif