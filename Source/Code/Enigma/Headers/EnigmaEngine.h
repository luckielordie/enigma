#ifndef ENIGMA_ENIGMAENGINE_H
#define ENIGMA_ENIGMAENGINE_H

#include <Window.h>

namespace Enigma
{
	class EnigmaEngine
	{
	private:
		Enigma::Graphics::Window* m_pWindow;
	public:
		EnigmaEngine();

		bool Initialise();
		void Run();
	};
};

#endif