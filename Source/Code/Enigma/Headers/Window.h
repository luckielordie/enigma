#ifndef ENIGMA_GRAPHICS_WINDOW_H
#define ENIGMA_GRAPHICS_WINDOW_H

#include <windows.h>
#include <Windows.h>
#include <string>

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
namespace Enigma
{
	namespace Graphics
	{
		class Window
		{
		private:
			HINSTANCE* m_pWindowInstance;
			HWND m_windowHandle;

			int m_windowWidth;
			int m_windowHeight;
			std::string m_windowTitle;

		public:
			Window(std::string windowTitle, int width, int height);
			bool InitialiseWindow();
		};
	}
}
#endif
