﻿using Networking.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Networking.Core.Connections;

namespace Networking.Client
{
	public abstract class Client
	{
        string hostname = "";
        public string Hostname { get { return hostname; } private set { hostname = value; } }

        protected Dictionary<string, Connection> connectionDictionary = new Dictionary<string, Connection>();

		public Client()
		{
		}

		public bool CreateConnection(ConnectionConfig config)
        {
            if (IsValidConfig(config))
            {
                switch (config.protocol)
                {
                    case ConnectionProtocol.TCP:
                        this.connectionDictionary.Add(config.connectionID, new TCPConnection(config.connectionID, config.connectIP, config.listenPort));
                        break;
                    case ConnectionProtocol.UDP:
                        this.connectionDictionary.Add(config.connectionID, new UDPConnection(config.connectionID, config.connectIP, config.listenPort, config.sendPort));
                        break;
                }

                if (this.connectionDictionary[config.connectionID].Connect())
                {
                    this.connectionDictionary[config.connectionID].SetReceivePacketCallback(this.OnRecievePacket);
                    this.connectionDictionary[config.connectionID].SetDisconnectedCallback(this.OnDisconnect);
                    this.OnConnect(config.connectionID);
                    return true;
                }
            }
            return false;
		}

        private bool IsValidConfig(ConnectionConfig config)
        {
            if (connectionDictionary.ContainsKey(config.connectionID))
            {
                Console.WriteLine("Connection ID already taken: {0}", config.connectionID);
                return false;
            }

            foreach(Connection connection in connectionDictionary.Values.ToList())
            {
                if(connection.Port == config.listenPort)
                {
                    Console.WriteLine("Connection Port already taken: {0}", config.listenPort);
                    return false;
                }
            }

            return true;
        }

		public void SendPacket(string connectionID, Packet packet)
		{
            if (connectionDictionary.ContainsKey(connectionID))
            {
                if (connectionDictionary[connectionID].Connected)
                {
                    this.connectionDictionary[connectionID].SendPacket(packet);
                }
                else
                {
                    CloseConnection(connectionID);
                }
            }
            
		}

        public void SendString(string connectionID, string stringToSend)
        {
            if (connectionDictionary.ContainsKey(connectionID))
            {
                if (connectionDictionary[connectionID].Connected)
                {
                    this.connectionDictionary[connectionID].SendString(stringToSend);
                else
                {
                    CloseConnection(connectionID);
                }
            }
        }

        public void CloseConnection(string connectionID)
		{
            if(connectionDictionary.ContainsKey(connectionID))
            {
                connectionDictionary.Remove(connectionID);
                this.OnDisconnect(connectionID.ToString());
            }
        }

        protected abstract void OnDisconnect(string connectionID);
        protected abstract void OnConnect(string connectionID);
        protected abstract void OnRecieveString(string recievedString);
	}
}
