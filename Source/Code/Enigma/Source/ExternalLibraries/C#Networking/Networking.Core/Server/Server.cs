﻿using Networking.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Networking.Server
{
	public abstract class Server
	{
        private Thread listenThread;
		private TcpListener tcpListener;

        protected Dictionary<Guid, ServerClient> connectedClients = new Dictionary<Guid, ServerClient>();

		private int tcpListenPort = 0;
        private int udpListenPort = 0;
		private string listenIp = "127.0.0.1";
        private bool serverListening = true;

		public Server(string listenIp, int tcpListenPort, int udpListenPort)
		{
			this.tcpListenPort = tcpListenPort;
            this.udpListenPort = udpListenPort;
			this.listenIp = listenIp;

			IPAddress ip = IPAddress.Parse(this.listenIp);
			this.tcpListener = new TcpListener(ip, this.tcpListenPort);

            listenThread = new Thread(new ThreadStart(Listen));
		}

        private void Listen()
        {
            this.tcpListener.Start();
            Console.WriteLine("Server Listening for connection on {0}:{1}", this.listenIp, this.tcpListenPort);
            serverListening = true;

            while (serverListening)
            {
                Socket newSocket = this.tcpListener.AcceptSocket();
                ServerClient newClient = new ServerClient(newSocket, this);


                connectedClients.Add(newClient.ID, newClient);

                this.OnClientConnect(newClient);
            }

            this.tcpListener.Stop();
        }

		public void StartListen()
		{
            listenThread.Start();
		}

        public abstract void OnClientConnect(ServerClient client);
        public abstract void ReceivePacket(string packet);
        public abstract void Update(float elapsedTime);

        public void StopListen()
        {
            serverListening = false;
        }

		public void CloseServer()
		{
		}
	}
}
