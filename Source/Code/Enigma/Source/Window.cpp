#include <Window.h>

using namespace Enigma::Graphics;
using namespace std;

Window::Window(string windowTitle, int width, int height)
{
	m_windowWidth = width;
	m_windowHeight = height;
	m_windowTitle = windowTitle;
}

bool Window::InitialiseWindow()
{
	HINSTANCE hInstance = GetModuleHandle(NULL);
	int nCmdShow = 5;
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = NULL;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = NULL;//(HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = m_windowTitle.c_str();
	wcex.hIconSm = NULL;
	if (!RegisterClassEx(&wcex))
		return false;

	// Create window
	this->m_pWindowInstance = &hInstance;
	RECT rc{ 0, 0, m_windowWidth, m_windowHeight };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	this->m_windowHandle = CreateWindow(m_windowTitle.c_str(), "",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
		NULL);
	if (!this->m_windowHandle)
		return false;

	ShowWindow(this->m_windowHandle, nCmdShow);

	return true;
}

//// Main message loop
//MSG msg = { 0 };
//while (WM_QUIT != msg.message)
//{
//	if (GetMessage(&msg, NULL, 0, 0, PM_REMOVE))
//	{
//		TranslateMessage(&msg);
//		DispatchMessage(&msg);
//	}
//	else
//	{
//		//MAIN LOOP ENTRY POINT
//	}
//}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'Q': PostQuitMessage(0); break;
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}