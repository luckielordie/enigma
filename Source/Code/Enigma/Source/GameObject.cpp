#include <GameObject.h>
#include <Component.h>

using namespace Enigma;
using namespace Enigma::Components;
using namespace std;

GameObject::GameObject(string objectTag)
{
	m_objectTag = objectTag;
	m_pComponentVector = new vector<Component*>();
}