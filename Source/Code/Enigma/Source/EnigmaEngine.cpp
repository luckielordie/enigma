#include <EnigmaEngine.h>

using namespace Enigma;

EnigmaEngine::EnigmaEngine()
{
	m_pWindow = new Graphics::Window("Enigma", 1600, 900);
}

bool EnigmaEngine::Initialise()
{
	if (!m_pWindow->InitialiseWindow()) return false;

	return true;
}

void EnigmaEngine::Run()
{
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			//MAIN LOOP ENTRY POINT
		}
	}
}