﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core.Connections
{
    public enum ConnectionProtocol
    {
        TCP,
        UDP
    }

    [Serializable]
    public class ConnectionConfig
    {
        public string connectionID;
        public string connectIP;
        public ConnectionProtocol protocol;
        public int listenPort;
        public int sendPort;

        public ConnectionConfig(string connectionID, ConnectionProtocol protocol, string connectIP, int listenPort, int sendPort)
        {
            this.connectionID = connectionID;
            this.protocol = protocol;
            this.connectIP = connectIP;
            this.listenPort = listenPort;
            this.sendPort = sendPort;
        }
    }
}
