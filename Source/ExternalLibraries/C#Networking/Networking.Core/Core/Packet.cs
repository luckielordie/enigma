﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core
{
	[Serializable]
	public class Packet
	{
		private string typeID;
		private List<object> data;

        public string Type { get { return this.typeID; } private set { this.typeID = value; } }
		public List<object> Data
		{
			get
			{ 
				return this.data;
			}
		}

        public Packet(string typeID, params object[] data)
        {
            this.data = new List<object>();
            foreach (object o in data)
            {
                this.data.Add(o);
            }

            this.Type = typeID;
        }

        public Packet(string typeID)
        {
            this.data = null;
            this.Type = typeID;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nTYPE: ");
            sb.Append("'" + this.typeID.ToString() + "'");

            if (this.data != null && this.data.Count > 0)
            {
                sb.Append("\nDATA");
                foreach (object o in this.data)
                {
                    sb.Append("\n  ");
                    sb.Append("DATA VALUE: ");
                    sb.Append(o.ToString());
                }
            }

            return sb.ToString();
        }
	}
}
