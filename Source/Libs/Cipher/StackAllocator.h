#ifndef CIPHER_PAGEMANAGEMENT_ALLOCATORS_STACKALLOCATOR_H
#define CIPHER_PAGEMANAGEMENT_ALLOCATORS_STACKALLOCATOR_H

#include <Allocator.h>
#include <FreeList.h>

namespace Cipher
{
	namespace PageManagement
	{
		namespace Allocators
		{
			class __declspec(dllexport) StackAllocator : public Allocator
			{
			private:
				struct AllocHeader
				{
					AllocHeader* m_pPreviousHeader;
					size_t adjustment;
				};

				FreeList<AllocHeader>* m_pHeaderList;
				AllocHeader* m_pTopStackHeader;


				uintptr_t m_pCurrentPosition;
			public:
				StackAllocator(Cipher::PageManagement::Page* page);
				~StackAllocator();
				inline uintptr_t Allocate(size_t size, size_t alignment);
				inline void Free(void* ptr);
			};
		}
	}
}

#endif