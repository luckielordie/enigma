#include <LinearAllocator.h>

using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;

LinearAllocator::LinearAllocator(Page* pageTop) : Allocator(pageTop)
{
	m_pCurrent = m_pPage->GetPageTopPointer();
}

inline uintptr_t LinearAllocator::Allocate(size_t size, size_t alignment)
{
	size_t adjustment = 0;

	if (alignment > 0)
	{
		adjustment = Allocator::GetAdjustment(m_pCurrent, alignment);
	}

	//check size of allocation not out of bounds
	if ((unsigned char*)m_pCurrent + adjustment + size >
		(unsigned char*)m_pPage->GetPageBottomPointer())
	{
		return 0;
	}

	//align the address
	uintptr_t address = m_pCurrent + adjustment;
	//add space onto page
	m_pCurrent = address + size;

	return address;
}

inline void LinearAllocator::Free(void* ptr)
{
	m_pCurrent = m_pPage->GetPageTopPointer();
}