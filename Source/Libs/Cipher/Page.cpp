#include <Page.h>
#include <Allocator.h>

using namespace std;
using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;

Page::Page(string pageName, size_t pageSize)
{
	m_pageName = pageName;
	m_pageSize = pageSize;

	m_pPageTop = reinterpret_cast<uintptr_t>(malloc((m_pageSize + 1)));
	m_pPageBottom = m_pPageTop + m_pageSize;
	
}

Page::~Page()
{
	free(reinterpret_cast<void*>(m_pPageTop));
	delete m_pAllocator;
	m_pAllocator = nullptr;
}

uintptr_t Page::GetPageTopPointer()
{
	return m_pPageTop;
}

uintptr_t Page::GetPageBottomPointer()
{
	return m_pPageBottom;
}

string Page::GetPageName()
{
	return m_pageName;
}

size_t Page::GetPageSize()
{
	return m_pageSize;
}

void Page::SetAllocator(Allocator* allocator)
{
	m_pAllocator = allocator;
}

bool Page::IsPointerInPage(void* ptr)
{
	uintptr_t charPtr = reinterpret_cast<uintptr_t>(ptr);
	return (charPtr >= m_pPageTop && charPtr <= m_pPageBottom);
}

inline uintptr_t Page::Allocate(size_t size, size_t alignment)
{
	return m_pAllocator->Allocate(size, alignment);
}
inline void Page::Free(void* addressToFree)
{
	m_pAllocator->Free(addressToFree);
}