#ifndef CIPHER_PAGEMANAGEMENT_ALLOCATORS_LINEARALLOCATOR_H
#define CIPHER_PAGEMANAGEMENT_ALLOCATORS_LINEARALLOCATOR_H

#include <stdlib.h>
#include <Allocator.h>
#include <Page.h>

namespace Cipher
{
	namespace PageManagement
	{
		namespace Allocators
		{
			class __declspec(dllexport) LinearAllocator : public Allocator
			{
			private:
				uintptr_t m_pCurrent;
			public:
				LinearAllocator(Page* pageTop);
				inline uintptr_t Allocate(size_t size, size_t alignment);
				inline void Free(void* ptr);
			};
		}
	}
}

#endif