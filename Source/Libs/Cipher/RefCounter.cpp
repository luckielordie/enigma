#include <RefCounter.h>

using namespace Cipher;

RefCounter::RefCounter()
{
	m_count = 0;
}

void RefCounter::AddRef()
{
	m_count++;
}

int RefCounter::Release()
{
	m_count--;
	return m_count;
}

void RefCounter::Reset()
{
	m_count = 0;
}