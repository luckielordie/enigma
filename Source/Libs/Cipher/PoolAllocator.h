#ifndef CIPHER_PAGEMANAGEMENT_ALLOCATORS_POOLALLOCATOR_H
#define CIPHER_PAGEMANAGEMENT_ALLOCATORS_POOLALLOCATOR_H

#include <Allocator.h>

namespace Cipher
{
	namespace PageManagement
	{
		namespace Allocators
		{
			class __declspec(dllexport) PoolAllocator : public Allocator
			{
			private:
				int m_objectSize;
				int m_objectAlignment;
				void** m_pFreeList;
			public:
				PoolAllocator(Cipher::PageManagement::Page* page, size_t objectSize, size_t objectAlignment);
				inline uintptr_t Allocate(size_t size, size_t alignment);
				inline void Free(void* ptr);
			};
		}
	}
}

#endif