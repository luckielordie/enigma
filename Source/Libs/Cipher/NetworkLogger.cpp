#include <NetworkLogger.h>

#define ASIO_STANDALONE
#define BOOST_ASIO_NO_WIN32_LEAN_AND_MEAN 
#include <asio-1.10.4\asio.hpp>
#include <iostream>

using namespace Cipher::Logging;
using namespace std;
using namespace asio;


NetworkLogger::NetworkLogger() : Logger()
{
	string hostname = "127.0.0.1";
	string port = "4414";	

	try
	{
		io_service ioService;
		ip::tcp::resolver resolver(ioService);
		ip::tcp::resolver::query query(hostname, port);
		ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		m_pSocket = new ip::tcp::socket(ioService);

		connect((*m_pSocket), endpoint_iterator);
	}
	catch (exception& e)
	{
		cerr << e.what() << endl;
	}
}

void NetworkLogger::SaveLog()
{
	try
	{
		write((*m_pSocket), buffer(*m_pDataBuffer));
	}
	catch (exception& e)
	{
		cerr << e.what() << endl;
	}

	m_pDataBuffer->clear();
}