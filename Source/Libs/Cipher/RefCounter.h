#ifndef CIPHER_REFCOUNTER_H
#define CIPHER_REFCOUNTER_H

namespace Cipher
{
	class RefCounter
	{
	private:
		int m_count;

	public:
		RefCounter();
		void Reset();
		void AddRef();
		int Release();
	};
}

#endif