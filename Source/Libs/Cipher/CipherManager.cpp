#include <CipherManager.h>

using namespace Cipher;
using namespace Cipher::PageManagement;
using namespace Cipher::Logging;
using namespace std;

CipherManager::CipherManager()
{

}

CipherManager::~CipherManager()
{

}

CipherManager& CipherManager::Instance()
{
	static CipherManager instance;
	return instance;
}

void CipherManager::Initialise()
{
	m_pLogger = new XMLLogger();
}

void CipherManager::SnapShotStart()
{
#if _DEBUG
	//check for end Probe
	if (m_logSnapshot == true)
	{
		m_logSnapshot = false;
		//LOG SNAPSHOT END PROBE NOT FOUND
		return;
	}
	m_logSnapshot = true;
#endif
}

void CipherManager::SnapShotEnd()
{
#if _DEBUG
	m_logSnapshot = false;
	//send snapshot
	m_pLogger->SaveLog();
#endif
}

inline void* CipherManager::Allocate(string pageName, const char* allocationType, size_t allocationSize, size_t allocationAlignment, const char* fileName, const int lineNumber, const char* functionName)
{
#if _DEBUG
	//RUN LOGGING IN HERE
	if (m_logSnapshot == true)
	{

		m_pLogger->AddAllocation(pageName, allocationType, allocationSize, allocationAlignment, string(fileName), lineNumber, string(functionName));
		//stringstream ss;
		////LOG ALLOC DETAILS
		//ss << "PAGE_NAME: " << "\"" << pageName << "\"";
		//ss << ", ALLOC_TYPE: " << "\"" << allocationType << "\"";
		//ss << ", ALLOC_SIZE: " << "\"" << allocationSize << "\"";
		//ss << ", ALLOC_ALIGN: " << "\"" << allocationAlignment << "\"";
		//ss << ", ALLOC_FILE: " << "\"" << fileName << "\"";
		//ss << ", ALLOC_LINE: " << "\"" <<lineNumber << "\"";
		//ss << ", ALLOC_FUNC: " << "\"" << functionName << "\"";
		//ss << '\n';

		//m_pLogger->AddValue(ss.str());
	}
#endif

	return PageManager::Instance().Allocate(pageName, allocationSize, allocationAlignment);
}

void CipherManager::FreePage(string pageName)
{
	PageManager::Instance().FreePage(pageName);
}

inline void CipherManager::Free(void* ptr)
{
	PageManager::Instance().Free(ptr);
}