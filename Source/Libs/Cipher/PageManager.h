#ifndef CIPHER_PAGEMANAGEMENT_PAGEMANAGER_H
#define CIPHER_PAGEMANAGEMENT_PAGEMANAGER_H

#include <vector>
#include <Page.h>

//include allocator headers
#include <LinearAllocator.h>
#include <StackAllocator.h>
#include <PoolAllocator.h>

#include <stdlib.h>

namespace Cipher
{
	namespace PageManagement
	{
		class __declspec(dllexport) PageManager
		{
		private:
			//prevent allocation and deallocation from outside the class
			PageManager();
			~PageManager();
			PageManager(PageManager const& m){};
			void operator =(PageManager const&){};

			std::vector<Page*>* m_pPageList;

			inline Page* GetPagePointer(std::string pageName);
			Page* GetPagePointer(void* pointer);
		public:
			//create and return instance
			static PageManager& Instance();

			inline void* Allocate(std::string pageName, size_t allocationSize, size_t allocationAlignment);
			inline void Free(void* pointer);

			void FreeAllPages();
			void FreePage(std::string pageName);

			template<class Allocator>
			void CreatePage(std::string pageName, size_t pageSize)
			{
				Page* page = new Page(pageName, pageSize);
				page->SetAllocator(new Allocator(page));
				m_pPageList->push_back(page);
			}
		};
	}
}

#endif