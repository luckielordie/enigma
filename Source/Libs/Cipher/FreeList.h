#ifndef CIPHER_PAGEMANAGEMENT_FREELIST_H
#define CIPHER_PAGEMANAGEMENT_FREELIST_H

#include <Windows.h>
//FreeList code from : https://jfdube.wordpress.com/2011/10/06/memory-management-part-2-allocations-tracking/
namespace Cipher
{
	namespace PageManagement
	{
		template<class T>
		class FreeList
		{
		private:
			T* m_pFreeList;
		public:
			FreeList()
			{
				m_pFreeList = nullptr;
			}

			~FreeList()
			{

			}

			inline T* Alloc()
			{
				if (m_pFreeList == nullptr)
				{
					const unsigned int requestAllocSize = 128000; //16KB request size from the OS
					const unsigned int numAllocPerBatch = requestAllocSize / sizeof(T);

					T* allocBatch = (T*)VirtualAlloc(NULL, requestAllocSize, MEM_COMMIT, PAGE_READWRITE);

					for (unsigned int i = 0; i < numAllocPerBatch; i++)
					{
						Free(allocBatch++);
					}
				}

				T* result = m_pFreeList;
				m_pFreeList = *((T**)m_pFreeList);
				return result;
			}

			inline void Free(T* ptr)
			{
				*(T**)ptr = m_pFreeList;
				m_pFreeList = ptr;
			}
		};
	}
}

#endif