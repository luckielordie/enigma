#include <PageManager.h>

using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;
using namespace std;

PageManager& PageManager::Instance()
{
	static PageManager instance;
	return instance;
}

PageManager::PageManager()
{
	m_pPageList = new vector<Page*>();
}

PageManager::~PageManager()
{
	FreeAllPages();

	delete m_pPageList;
	m_pPageList = nullptr;
}

void PageManager::FreeAllPages()
{
	for (std::vector<Page*>::iterator it = m_pPageList->begin(); it != m_pPageList->end(); it++)
	{
		delete *it;
		*it = nullptr;
	}

	m_pPageList->clear();
}

void PageManager::FreePage(string pageName)
{
	for (vector<Page*>::iterator it = m_pPageList->begin(); it != m_pPageList->end(); it++)
	{
		if (pageName == (*it)->GetPageName())
		{
			Page* pageToRemove = (*it);
			m_pPageList->erase(it);

			delete pageToRemove;
			pageToRemove = nullptr;
			break;
		}
	}
}

inline void* PageManager::Allocate(string pageName, size_t allocationSize, size_t allocationAlignment)
{
	Page* allocationPage = this->GetPagePointer(pageName);

	if (allocationPage != nullptr)
	{
		uintptr_t ptr = allocationPage->Allocate(allocationSize, allocationAlignment);

		if (ptr == 0)
		{
			//LOG FAILED ATTEMPT
			return nullptr;
		}

		return reinterpret_cast<void*>(ptr);
	}

	return nullptr;
}

inline void PageManager::Free(void* pointer)
{
	Page* page = this->GetPagePointer(pointer);
	if (page != nullptr)
	{
		page->Free(pointer);
	}
	else
	{
		//INVALID ADDRESS
		delete pointer;
	}
}

Page* PageManager::GetPagePointer(string pageName)
{
	for (std::vector<Page*>::iterator it = m_pPageList->begin(); it != m_pPageList->end(); it++)
	{
		if ((*it)->GetPageName() == pageName) return *it;
	}
	
	return nullptr;
}

Page* PageManager::GetPagePointer(void* pointer)
{
	for(std::vector<Page*>::iterator it = m_pPageList->begin(); it != m_pPageList->end(); it++)
	{
		Page* page = *it;
		if (page->IsPointerInPage(pointer))
		{
			return page;
		}
	}

	return nullptr;
}
