#ifndef CIPHER_CIPHERMANAGER_H
#define CIPHER_CIPHERMANAGER_H

#include <string>
#include <PageManager.h>
#include <NetworkLogger.h>
#include <PlainTextLogger.h>
#include <XMLLogger.h>
#include <sstream>

namespace Cipher
{
	class __declspec(dllexport) CipherManager
	{
	private:
		bool m_logSnapshot = false;
		Logging::XMLLogger* m_pLogger;

	private:
		CipherManager();
		~CipherManager();
		CipherManager(CipherManager const& m){};
		void operator =(CipherManager const&){};

	public:
		static CipherManager& Instance();

		inline void* Allocate(std::string pageName, const char* allocationType, size_t allocationSize, size_t allocationAlignment, const char* fileName, const int lineNumber, const char* functionName);
		inline void Free(void* pointer);

		void SnapShotStart();
		void SnapShotEnd();

		void Initialise();

		template<class Allocator>
		void CreatePage(std::string pageName, size_t pageSize)
		{
			PageManagement::PageManager::Instance().CreatePage<Allocator>(pageName, pageSize);
		}

		void FreePage(std::string pageName);
	};

#define CIPHER CipherManager::Instance()
}
#endif