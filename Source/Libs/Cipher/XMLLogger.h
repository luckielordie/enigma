#ifndef CIPHER_LOGGING_XMLLOGGER_H
#define CIPHER_LOGGING_XMLLOGGER_H
#include <Logger.h>

namespace tinyxml2
{
	class XMLDocument;
	class XMLElement;
}

namespace Cipher
{
	namespace Logging
	{
		class XMLLogger : public Logger
		{
		private:
			tinyxml2::XMLDocument* m_pDocument;
			tinyxml2::XMLElement* m_pRootElement;
			tinyxml2::XMLElement* m_pDocumentRoot;
			std::vector<tinyxml2::XMLElement*>* m_pNodeBuffer;
			int m_snapshotNumber = 0;
			bool m_newSnapshot = true;
		public:
			XMLLogger();
			void SaveLog();
			void AddAllocation(std::string pageName, std::string allocationType, size_t allocationSize,
				size_t allocationAlignment, std::string fileName, int lineNumber, std::string functionName);
		};
	}
}
#endif