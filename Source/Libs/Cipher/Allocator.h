#ifndef CIPHER_PAGEMANAGEMENT_ALLOCATORS_ALLOCATOR_H
#define CIPHER_PAGEMANAGEMENT_ALLOCATORS_ALLOCATOR_H

#include <Page.h>
#include <assert.h>
#include <cstdint>

namespace Cipher
{
	namespace PageManagement
	{
		namespace Allocators
		{
			class __declspec(dllexport) Allocator
			{
			protected:
				Page* m_pPage;
			protected:
				static size_t GetAdjustment(const uintptr_t address, size_t alignment);
				static size_t GetAdjustment(const uintptr_t address, size_t alignement, size_t headerSize);
				static void NullPage();
			public:
				Allocator(Page* pageToAllocate);

				virtual inline uintptr_t Allocate(size_t size, size_t alignment) = 0;
				virtual inline void Free(void* p) = 0;

			};
		}
	}
}
#endif