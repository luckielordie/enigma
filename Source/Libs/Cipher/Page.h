#ifndef CIPHER_PAGEMANAGEMENT_PAGE_H
#define CIPHER_PAGEMANAGEMENT_PAGE_H

#include <new>
#include <string>

namespace Cipher
{
	namespace PageManagement
	{
		//Forward declaration of Allocator class
		namespace Allocators{ class Allocator; }

		class __declspec(dllexport) Page
		{
		private:
			std::string m_pageName;
			uintptr_t m_pPageTop;
			uintptr_t m_pPageBottom;
			size_t m_pageSize;

			Allocators::Allocator* m_pAllocator;

		public:
			Page(std::string pageName, size_t pageSize);
			~Page();

			void SetAllocator(Allocators::Allocator* allocator);
			uintptr_t GetPageTopPointer();
			uintptr_t GetPageBottomPointer();
			size_t GetPageSize();
			inline std::string GetPageName();
			bool IsPointerInPage(void* ptr);

			inline uintptr_t Allocate(size_t size, size_t alignment);
			inline void Free(void* addressToFree);
		};
	}
}

#endif