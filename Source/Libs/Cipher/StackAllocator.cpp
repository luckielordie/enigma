#include <StackAllocator.h>

using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;

StackAllocator::StackAllocator(Page* page) : Allocator(page)
{
	m_pCurrentPosition = page->GetPageTopPointer();
	m_pHeaderList = new FreeList<AllocHeader>();
	m_pTopStackHeader = nullptr;
}

StackAllocator::~StackAllocator()
{
	delete m_pHeaderList;
	m_pHeaderList = nullptr;
}

inline uintptr_t StackAllocator::Allocate(size_t size, size_t alignment)
{
	size_t adjustment = 0;

	if (alignment > 0)
	{
		adjustment = Allocator::GetAdjustment(m_pCurrentPosition, alignment);
	}

	if (m_pCurrentPosition + adjustment + size > m_pPage->GetPageBottomPointer()) return 0;

	uintptr_t alignedAddress = m_pCurrentPosition + adjustment;

	//create header on free list then link top ref to it
	AllocHeader* header = m_pHeaderList->Alloc();
	header->adjustment = adjustment + size;
	header->m_pPreviousHeader = m_pTopStackHeader;
	m_pTopStackHeader = header;
	
	m_pCurrentPosition = alignedAddress + size;
	
	return alignedAddress;
}

inline void StackAllocator::Free(void* ptr)
{
	uintptr_t address = reinterpret_cast<uintptr_t>(ptr);
	m_pCurrentPosition = address - m_pTopStackHeader->adjustment;
	
	//free header
	AllocHeader* tempTop = m_pTopStackHeader;
	m_pTopStackHeader = m_pTopStackHeader->m_pPreviousHeader;
	m_pHeaderList->Free(tempTop);
}