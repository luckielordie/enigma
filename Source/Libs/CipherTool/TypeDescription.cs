﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherTool
{
    public class TypeDescription
    {
        public string Name
        {
            get;
            private set;
        }

        public int Size
        {
            get;
            private set;
        }

        public TypeDescription(string typeName, int allocSize)
        {
            Name = typeName;
            Size = allocSize;
        }

        public void AddSize(int amount)
        {
            Size += amount;
        }

        public override string ToString()
        {
            return "TYPE: " + Name + " SIZE: " + Size + "B";
        }
    }
}
