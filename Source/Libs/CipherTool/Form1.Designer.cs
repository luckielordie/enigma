﻿namespace CipherTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabControl = new System.Windows.Forms.TabPage();
            this.snapshotTreeView = new System.Windows.Forms.TreeView();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.compareSnapshotButton = new System.Windows.Forms.Button();
            this.snapshotButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.typeTreeView = new System.Windows.Forms.TreeView();
            this.tabControl1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabControl);
            this.tabControl1.Location = new System.Drawing.Point(12, 36);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(992, 763);
            this.tabControl1.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.snapshotTreeView);
            this.tabControl.Location = new System.Drawing.Point(4, 22);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Windows.Forms.Padding(3);
            this.tabControl.Size = new System.Drawing.Size(984, 737);
            this.tabControl.TabIndex = 1;
            this.tabControl.Text = "Snapshots";
            this.tabControl.UseVisualStyleBackColor = true;
            this.tabControl.Click += new System.EventHandler(this.tabControl_Click);
            // 
            // snapshotTreeView
            // 
            this.snapshotTreeView.CheckBoxes = true;
            this.snapshotTreeView.Location = new System.Drawing.Point(3, 3);
            this.snapshotTreeView.Name = "snapshotTreeView";
            this.snapshotTreeView.Size = new System.Drawing.Size(977, 734);
            this.snapshotTreeView.TabIndex = 0;
            this.snapshotTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.snapshotTreeView_AfterSelect);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(1010, 58);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(417, 384);
            this.propertyGrid1.TabIndex = 1;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.Click += new System.EventHandler(this.propertyGrid1_Click);
            // 
            // compareSnapshotButton
            // 
            this.compareSnapshotButton.Location = new System.Drawing.Point(1010, 734);
            this.compareSnapshotButton.Name = "compareSnapshotButton";
            this.compareSnapshotButton.Size = new System.Drawing.Size(131, 61);
            this.compareSnapshotButton.TabIndex = 2;
            this.compareSnapshotButton.Text = "Compare Snapshots";
            this.compareSnapshotButton.UseVisualStyleBackColor = true;
            this.compareSnapshotButton.Click += new System.EventHandler(this.compareSnapshotButton_Click);
            // 
            // snapshotButton
            // 
            this.snapshotButton.Location = new System.Drawing.Point(1147, 734);
            this.snapshotButton.Name = "snapshotButton";
            this.snapshotButton.Size = new System.Drawing.Size(142, 61);
            this.snapshotButton.TabIndex = 3;
            this.snapshotButton.TabStop = false;
            this.snapshotButton.Text = "Capture Snapshot";
            this.snapshotButton.UseVisualStyleBackColor = true;
            this.snapshotButton.Click += new System.EventHandler(this.snapshotButton_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1295, 734);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(131, 61);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // typeTreeView
            // 
            this.typeTreeView.Location = new System.Drawing.Point(1010, 458);
            this.typeTreeView.Name = "typeTreeView";
            this.typeTreeView.Size = new System.Drawing.Size(416, 270);
            this.typeTreeView.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1439, 811);
            this.Controls.Add(this.typeTreeView);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.snapshotButton);
            this.Controls.Add(this.compareSnapshotButton);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabControl;
        private System.Windows.Forms.TreeView snapshotTreeView;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Button compareSnapshotButton;
        private System.Windows.Forms.Button snapshotButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TreeView typeTreeView;
    }
}

