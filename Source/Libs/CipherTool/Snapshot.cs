﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherTool
{
    public class Snapshot
    {
        #region PROPERTIES
        [Category("Snapshot")]
        [DisplayName("Snapshot ID")]       
        [Description("A unique ID for the snapshot.")]
        [ReadOnly(true)]
        public Guid ID
        {
            get;
            private set;
        }

        [Category("Allocations")]
        [DisplayName("Allocation")]
        [Description("An Allocation made in the application.")]
        [ReadOnly(true)]
        public List<Allocation> Allocations
        {
            get;
            private set;
        }

        [Category("Snapshot")]
        [DisplayName("Number of Allocations")]
        [Description("The total number of allocations made in this snapshot.")]
        [ReadOnly(true)]
        public int NumberOfAllocations
        {
            get { return Allocations.Count; }
            private set{}
        }

        public List<TypeDescription> SnapshotTypes
        {
            get { return typeManager.TypeList; }
            private set { }
        }
        #endregion

        private TypeHandler typeManager;
        public Snapshot()
        {
            ID = Guid.NewGuid();
            typeManager = new TypeHandler();            
        }

        public void SetAllocs(List<Allocation> allocations)
        {
            Allocations = allocations;
            typeManager.RegisterTypes(Allocations);
        }

        public TypeDescription GetTypeDescription(string typeName)
        {
            return typeManager.GetTypeDescription(typeName);
        }

        public static List<Allocation> GetDiffAllocations(Snapshot firstSnapshot, Snapshot secondSnapshot)
        {
            List<Allocation> diffAllocations = new List<Allocation>();
            diffAllocations = firstSnapshot.Allocations.Intersect(secondSnapshot.Allocations).ToList();

            return firstSnapshot.Allocations;
        }

        public override string ToString()
        {
            return "SNAPSHOT [ ID: " + this.ID + " NUMBER OF ALLOCATIONS: " + this.NumberOfAllocations + " ]";
        }

    }
}
