﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherTool
{
    public class TypeHandler
    {
        private List<TypeDescription> typeList;

        public List<TypeDescription> TypeList
        {
            get { return typeList; }
            private set { }
        }

        public TypeHandler()
        {
            typeList = new List<TypeDescription>();
        }

        public TypeDescription GetTypeDescription(string typeID)
        {
            foreach(TypeDescription type in typeList)
            {
                if(type.Name == typeID)
                {
                    return type;
                }
            }

            return new TypeDescription("NO TYPE", 0);
        }

        public void RegisterTypes(List<Allocation> allocationList)
        {
            foreach (Allocation allocation in allocationList)
            {
                bool typeExists = false;
                foreach (TypeDescription desc in typeList)
                {
                    if (desc.Name == allocation.Type)
                    {
                        typeExists = true;
                        desc.AddSize(allocation.Size);
                    }
                }

                if(!typeExists)
                {
                    TypeDescription newDesc = new TypeDescription(allocation.Type, allocation.Size);
                    typeList.Add(newDesc);
                }                
            }
        }
    }
}
