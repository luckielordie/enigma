﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CipherTool
{
    public static class SnapshotTreeController
    {
        private static Snapshot m_snapshot1 = null;
        private static Snapshot m_snapshot2 = null;

        public static void UpdateTreeControl(TreeView treeView)
        {
            treeView.Nodes.Clear();

            //@"C:\Users\luckielordie\Documents\visual studio 2013\Projects\MemManagerStaffs\TestProject\snapshot.xml"
            List<Snapshot> snapshots = null;

            treeView.MouseClick += (sender, args) =>
                {
                    TreeNode nodeClicked = treeView.GetNodeAt(args.X, args.Y);

                    if (nodeClicked == null) return;

                    if(nodeClicked.Tag is Snapshot)
                    {
                        m_snapshot2 = m_snapshot1;
                        m_snapshot1 = nodeClicked.Tag as Snapshot;
                    }

                    return;
                };
            

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (sender, args) => { snapshots = XMLSnapshotParser.ParseFile("snapshot.xml"); };
            bw.RunWorkerCompleted += (sender, args) =>
            {
                if (args.Error != null) { MessageBox.Show(args.Error.ToString()); }

                UpdateTree(snapshots, treeView);
            };

            bw.RunWorkerAsync();
        }

        public static List<Snapshot> GetSelectedSnapshots()
        {
            return new List<Snapshot>(){ m_snapshot1, m_snapshot2};
        }

        public static void UpdateTree(List<Snapshot> snapshots, TreeView treeView)
        {
            treeView.Nodes.Clear();
            foreach (Snapshot snapshot in snapshots)
            {
                List<TreeNode> allocationNodes = new List<TreeNode>();

                foreach (Allocation allocation in snapshot.Allocations)
                {
                    TreeNode allocationNode = new TreeNode(allocation.ToString());
                    allocationNode.Tag = allocation;
                    allocationNodes.Add(allocationNode);
                }

                TreeNode snapshotNode = new TreeNode(snapshot.ToString(), allocationNodes.ToArray());
                snapshotNode.Tag = snapshot;
                treeView.Nodes.Add(snapshotNode);
            }
        }

        public static void UpdateTree(List<Allocation> allocations, TreeView treeView)
        {
            treeView.Nodes.Clear();
            
            foreach(Allocation allocation in allocations)
            {
                treeView.Nodes.Add(new TreeNode(allocation.ToString()){ Tag = allocation });
            }
        }
    }
}
